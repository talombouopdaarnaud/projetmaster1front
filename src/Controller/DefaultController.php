<?php

declare(strict_types=1);

namespace App\Controller;
use Symfony\Component\HttpFoundation\RequestStack;
use Qipsius\TCPDFBundle\Controller\TCPDFController;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends AbstractController
{

 
    private $api_server;
   
    protected $tcpdf;
    protected $requestStack;
    private $userService;


    public function __construct(
        //TCPDFController $tcpdf,
        HttpClientInterface $client,
        //UserService $userService,
        RequestStack $requestStack
    ) {
        //$this->tcpdf = $tcpdf;
        $this->client = $client;
        //$this->userService = $userService;
        $this->requestStack = $requestStack;
    }

    public $protected = array("success" => false, "message" => "", 'link' => "", "data" => null);

    public function getApiData($url,$data=[])
    {

            $response = $this->client->request('GET',"http://127.0.0.1:8001/api". $url, $data);

            $content = $response->toArray();
            return $content;

    }


    public function SendApiData($method, $url, $data)
    {
        $response =  $this->client->request($method,"http://127.0.0.1:8001/api".$url, $body);
        //$this->resetUserInSession();
        if ($method === 'DELETE') {
            return $response;
        } else {
            return $response->toArray();
        }
    }

    public function returnPDFResponseFromHTML($html, $filename)
    {
       
    }


    public function decodeToken($token)
    {
       
    }

    public function isLogged()
    {
        
    }

    public function getCurrentUser()
    {
       
    }

    public function resetUserInSession(){
      
    }

    public function printRecu($entree){
        
    }
}
