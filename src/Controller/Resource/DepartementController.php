<?php

declare(strict_types=1);

namespace App\Controller\Resource;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\DefaultController;


class DepartementController extends DefaultController
{


    /**
     * @Route("/departementindex", name="departement-index", methods={"GET"})
     */
    public function index()
    {
        //$data=$this->getApiData("/composants");
        //dd($data);
        return $this->render('Departement/index.html.twig', []);
    }
}
